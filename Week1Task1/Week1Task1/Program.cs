﻿using System;
/*Task 1 && 2*/
namespace Week1Task1
{
    class Program
    {
        public static bool isVowel(char input) /*Checking if input is a vowel*/
        {
            char[] vowel = { 'a', 'e', 'i', 'y', 'o', 'u' };
            foreach (char c in vowel)
            {
                if (input.ToString().ToLower()[0].Equals(c))
                {
                    return true;
                }
            }
            return false;
        }
        static string aOrAn(char input)/*Fuction for returning 'a' or 'an' depending on if input is a vowel or not*/
        {
            return isVowel(input) ? "an" : "a";
        }
        static void Main()
        {
            Console.WriteLine("What is your name?");
            String name = Console.ReadLine().Trim();
            Console.WriteLine($"Your name is: {name}!");
            Console.WriteLine($"Yor name contains {name.Length} letters. It starts with {aOrAn(name[0])} {name[0]}");
        }
    }
}
